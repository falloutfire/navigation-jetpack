package com.example.androidnavigation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_splash.view.*

/**
 * A simple [Fragment] subclass.
 */
class SplashFragment : Fragment() {

    lateinit var controller: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_splash, container, false)

        controller = Navigation.findNavController(activity!!, R.id.main_nav_host_fragment)


        view.buttonPinCode.setOnClickListener {
            controller.navigate(R.id.action_splashFragment_to_pinCodeFragment)
        }

        view.buttonPinCodeLogin.setOnClickListener {
            controller.navigate(R.id.action_splashFragment_to_loginFragment)
        }

        return view
    }

}
