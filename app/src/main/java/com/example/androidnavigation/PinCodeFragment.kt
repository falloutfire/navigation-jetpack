package com.example.androidnavigation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_pin_code.view.*

/**
 * A simple [Fragment] subclass.
 */
class PinCodeFragment : Fragment() {

    lateinit var controller: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val view = inflater.inflate(R.layout.fragment_pin_code, container, false)

        controller = Navigation.findNavController(activity!!, R.id.main_nav_host_fragment)

        view.buttonLogin.setOnClickListener {
            controller.navigate(R.id.action_pinCodeFragment_to_loginFragment)
        }

        view.buttonEnter.setOnClickListener {
            controller.navigate(R.id.action_pinCodeFragment_to_blankFragment3)
        }
        return view
    }

}
