package com.example.androidnavigation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.androidnavigation.R
import kotlinx.android.synthetic.main.home_fragment.view.*
import kotlinx.android.synthetic.main.settings_fragment.view.*

class SettingsFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)

        val view = inflater.inflate(R.layout.settings_fragment, container, false)
        view.button2.setOnClickListener {
            val controller = Navigation.findNavController(activity!!, R.id.main_nav_host_fragment)
            controller.navigate(R.id.action_blankFragment3_to_blankFragment222)
        }

        return view
    }
}