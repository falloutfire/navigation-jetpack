package com.example.androidnavigation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_create_pin.view.*
import kotlinx.android.synthetic.main.fragment_login.view.*

/**
 * A simple [Fragment] subclass.
 */
class CreatePinFragment : Fragment() {

    lateinit var controller: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_create_pin, container, false)

        controller = Navigation.findNavController(activity!!, R.id.main_nav_host_fragment)

        view.buttonGeneratePin.setOnClickListener {
            controller.navigate(R.id.action_createPinFragment_to_blankFragment3)
        }

        return view
    }

}
